package com.tokbox.android.accelerator.sample.call.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.tokbox.android.accelerator.sample.R;
import com.tokbox.android.accelerator.sample.call.config.OpenTokConfig;
import com.tokbox.android.accelerator.sample.call.oplogic.BaseOpenTok;
import com.tokbox.android.accelerator.sample.call.otokchat.ChatMessage;
import com.tokbox.android.accelerator.sample.call.otokchat.TextChatFragment;

/**
 * Created by Pedro Gomez on 08/06/2018.
 */

public class BaseCallActivity extends AppCompatActivity implements BaseOpenTok.CallStateListener, TextChatFragment.TextChatListener, ActionBar.PreviewControlCallbacks {


    protected BaseOpenTok baseOpenTok;

    private final String[] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private final int permsRequestCode = 200;
    //Permissions
    private boolean mAudioPermission = false;
    private boolean mVideoPermission = false;
    private boolean mWriteExternalStoragePermission = false;
    private boolean mReadExternalStoragePermission = false;
    protected FrameLayout mTextChatContainer;
    protected ActionBar actionBar;

    protected TextChatFragment mTextChatFragment;
    protected TypeCall type;

    protected enum TypeCall {

        VIDEOCALL, VOICECALL, CHATCALL;
    }

    public BaseCallActivity(TypeCall type) {
        this.type = type;
    }

    public BaseCallActivity() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ContextCompat.checkSelfPermission(this, permissions[1]) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, permissions[0]) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, permsRequestCode);
            }
        } else {
            mVideoPermission = true;
            mAudioPermission = true;
            mWriteExternalStoragePermission = true;
            mReadExternalStoragePermission = true;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (baseOpenTok != null)
            baseOpenTok.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (baseOpenTok != null)
            baseOpenTok.onResume();
    }

    @Override
    public void onRequestPermissionsResult(final int permsRequestCode, final String[] permissions,
                                           int[] grantResults) {
        switch (permsRequestCode) {
            case 200:
                mVideoPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                mAudioPermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                mReadExternalStoragePermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                mWriteExternalStoragePermission = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                if (!mVideoPermission || !mAudioPermission || !mReadExternalStoragePermission || !mWriteExternalStoragePermission) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(BaseCallActivity.this);
                    builder.setTitle(getResources().getString(R.string.permissions_denied_title));
                    builder.setMessage(getResources().getString(R.string.alert_permissions_denied));
                    builder.setPositiveButton("I'M SURE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.setNegativeButton("RE-TRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permissions, permsRequestCode);
                            }
                        }
                    });
                    builder.show();
                }
                break;
        }
    }

    public void warningNetworkQuality() {

    }

    protected void initTextChatFragment() {
        String chatName;
        if (getIntent() != null && getIntent().getExtras() != null && getIntent().getStringExtra("CHATNAME") != null)
            chatName = getIntent().getStringExtra("CHATNAME");
        else
            chatName = "sin nombre";

        mTextChatFragment = TextChatFragment.newInstance(baseOpenTok.getWrapper().getSession(), OpenTokConfig.API_KEY);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.textchat_fragment_container, mTextChatFragment).commit();
        getSupportFragmentManager().executePendingTransactions();
        try {
            mTextChatFragment.setSenderAlias(chatName);
            mTextChatFragment.setMaxTextLength(140);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mTextChatFragment.setListener(this);

    }

    @Override
    public void onNewSentMessage(ChatMessage message) {

    }

    @Override
    public void onNewReceivedMessage(ChatMessage message) {

    }

    @Override
    public void onTextChatError(String error) {

    }

    @Override
    public void onClosed() {
        mTextChatContainer.setVisibility(View.GONE);
    }

    @Override
    public void onRestarted() {

    }

    @Override
    public void onBackPressed() {
        if (mTextChatContainer.getVisibility() == View.VISIBLE && type != TypeCall.CHATCALL) {
            mTextChatContainer.setVisibility(View.GONE);
            baseOpenTok.onResume();
        } else
            super.onBackPressed();
    }

    @Override
    public void onConnect() {
        Log.i("tag", "--->////////onConnect");
        initTextChatFragment();
    }

    @Override
    public void onConnected() {
        Log.i("tag", "--->////////onConnected");
        if (type != TypeCall.CHATCALL)
            actionBar.setEnabled(true);
        mTextChatFragment.init();
    }

    @Override
    public void onReconnecting() {
        Log.i("tag", "--->////////onReconnecting");

    }

    @Override
    public void onReconnected() {
        Log.i("tag", "--->////////onReconnected");
    }

    @Override
    public void onDisconnected() {
        Log.i("tag", "--->////////onDisconnected");
    }

    @Override
    public void onRemoteJoined() {
        Log.i("tag", "--->////////onRemoteJoined");
    }

    @Override
    public void onRemoteLeft() {
        Log.i("tag", "--->////////onRemoteLeft");
    }


    @Override
    public void onError() {
        Log.i("tag", "--->////////onError");
    }


    @Override
    public void onDisableLocalAudio(boolean audio) {
        baseOpenTok.enableLocalAudio(audio);
    }

    @Override
    public void onDisableLocalVideo(boolean video) {
        baseOpenTok.enableLocalVideo(video);
    }

    @Override
    public void onTextChat() {
        mTextChatContainer.setVisibility(View.VISIBLE);
        baseOpenTok.onPause();
    }

}
