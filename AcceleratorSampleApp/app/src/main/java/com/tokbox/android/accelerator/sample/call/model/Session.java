package com.tokbox.android.accelerator.sample.call.model;

/**
 * Created by Pedro Gomez on 08/06/2018.
 */

public class Session {

    public String SESSION_ID;
    public String TOKEN;

    public Session(String SESSION_ID, String TOKEN) {
        this.SESSION_ID = SESSION_ID;
        this.TOKEN = TOKEN;
    }

    public String getSESSION_ID() {
        return SESSION_ID;
    }

    public void setSESSION_ID(String SESSION_ID) {
        this.SESSION_ID = SESSION_ID;
    }

    public String getTOKEN() {
        return TOKEN;
    }

    public void setTOKEN(String TOKEN) {
        this.TOKEN = TOKEN;
    }
}
