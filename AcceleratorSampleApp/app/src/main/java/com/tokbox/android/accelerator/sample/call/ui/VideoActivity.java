package com.tokbox.android.accelerator.sample.call.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.tokbox.android.accelerator.sample.R;
import com.tokbox.android.accelerator.sample.call.config.OpenTokConfig;
import com.tokbox.android.accelerator.sample.call.model.Session;
import com.tokbox.android.accelerator.sample.call.oplogic.BaseOpenTok;

/**
 * Created by Pedro Gomez on 06/06/2018.
 */

public class VideoActivity extends BaseCallActivity implements BaseOpenTok.VideoCallListener {


    //Fragments and containers

    private RelativeLayout remoteStream;
    private RelativeLayout localStream;
    private ImageView avatar;

    public VideoActivity() {
        super(TypeCall.VIDEOCALL);
    }


    public static Intent intent(Context context, String chatName) {
        Intent intent = new Intent(context, VideoActivity.class);
        intent.putExtra("CHATNAME", chatName);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        avatar = new ImageView(this);


        baseOpenTok = new BaseOpenTok();
        baseOpenTok.init(this, this, new Session(OpenTokConfig.SESSION_ID, OpenTokConfig.TOKEN));

        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);
        localStream = (RelativeLayout) findViewById(R.id.rl_local_stream);
        remoteStream = (RelativeLayout) findViewById(R.id.rl_remote_stream);

        actionBar = (new ActionBar());
        actionBar.onAttach(this);
        actionBar.init(findViewById(R.id.actionBar), baseOpenTok,true);
    }

    @Override
    public void onRemoteVideoChanged(boolean videoActive) {
        Log.i("tag", "--->////////onRemoteVideoChanged  " +videoActive);
        if (!videoActive) {
            remoteStream.removeView(avatar);
            avatar.setImageDrawable(getDrawable(R.drawable.avatar));
            avatar.setScaleType(ImageView.ScaleType.FIT_XY);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
            avatar.setLayoutParams(params);

            remoteStream.addView(avatar, params);
        } else {
            remoteStream.removeView(avatar);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actionBar.onDetach();
    }

    @Override
    public void addRemoteStream(View remoteStream) {
        this.remoteStream.addView(remoteStream);
        onRemoteVideoChanged(true);
    }

    @Override
    public void addLocalStream(View localStream) {
        this.localStream.addView(localStream);
    }

    @Override
    public void removeRemoteStream() {
        this.remoteStream.removeAllViews();
        onRemoteVideoChanged(false);
    }

    @Override
    public void removeLocalStream() {
        this.localStream.removeAllViews();
    }

    @Override
    public void qualityAlert() {

    }

}
