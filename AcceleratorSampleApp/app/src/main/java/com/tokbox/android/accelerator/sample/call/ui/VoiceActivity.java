package com.tokbox.android.accelerator.sample.call.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.tokbox.android.accelerator.sample.R;
import com.tokbox.android.accelerator.sample.call.config.OpenTokConfig;
import com.tokbox.android.accelerator.sample.call.model.Session;
import com.tokbox.android.accelerator.sample.call.oplogic.BaseOpenTok;

/**
 * Created by Pedro Gomez on 06/06/2018.
 */

public class VoiceActivity  extends BaseCallActivity implements BaseOpenTok.VoiceCallListener{

    public VoiceActivity() {
        super(TypeCall.VOICECALL);
    }

    public static Intent intent(Context context, String chatName) {
        Intent intent = new Intent(context, VoiceActivity.class);
        intent.putExtra("CHATNAME", chatName);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice);


        baseOpenTok = new BaseOpenTok();
        baseOpenTok.init(this,this,new Session(OpenTokConfig.SESSION_ID,OpenTokConfig.TOKEN));

        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);

        actionBar = (new ActionBar());
        actionBar.onAttach(this);
        actionBar.init(findViewById(R.id.actionBar), baseOpenTok,false);
    }
}
