package com.tokbox.android.accelerator.sample.call.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.tokbox.android.accelerator.sample.R;
import com.tokbox.android.accelerator.sample.call.config.OpenTokConfig;
import com.tokbox.android.accelerator.sample.call.model.Session;
import com.tokbox.android.accelerator.sample.call.oplogic.BaseOpenTok;

/**
 * Created by Pedro Gomez on 06/06/2018.
 */

public class ChatActivity extends BaseCallActivity implements BaseOpenTok.ChatCallListener {


    public ChatActivity() {
        super(TypeCall.CHATCALL);
    }

    public static Intent intent(Context context, String chatName) {
        Intent intent = new Intent(context, ChatActivity.class);
        intent.putExtra("CHATNAME", chatName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        baseOpenTok = new BaseOpenTok();
        baseOpenTok.init(this, this, new Session(OpenTokConfig.SESSION_ID, OpenTokConfig.TOKEN));

        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);
        mTextChatContainer.setVisibility(View.VISIBLE);

    }

}
